apk update
apk add postgresql-client

PGPASSWORD=$PGPASSWORD psql -h $PGHOST -U $PGUSER -d $PGDATABASE -c "SELECT id FROM \"App\" WHERE \"path\" = 'teambarometer';" -tA | xargs -I {} npx appsemble group create --app-id {} --app apps/teambarometer "Test Group"
