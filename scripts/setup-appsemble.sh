npx appsemble organization create appsemble
npx appsemble organization create eindhoven

npx appsemble -vv block publish blocks/*
npx appsemble -vv app publish apps/teambarometer --modify-context
npx appsemble -vv app publish apps/swimmingpools --modify-context
npx appsemble -vv app publish apps/werkplek-reservering --modify-context

. scripts/setup-group.sh
