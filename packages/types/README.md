# Types

This package contains reusable types definitions for this repository.

## LICENSE

[LGPL-3.0-only](../../LICENSE.md) © [Appsemble](https://appsemble.com)
