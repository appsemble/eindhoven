export interface MaintenanceWindow {
  /**
   * The start datetime of the maintenance window.
   */
  start: string;

  /**
   * The end datetime of the maintenance window.
   */
  end: string;

  /**
   * The available capacity during this maintenance window.
   */
  capacity: number;
}

/**
 * The options allowed for desk types.
 */
export type DeskType =
  | 'Cockpit / Standaard'
  | 'Cockpit / Zit-Sta'
  | 'Open / Standaard'
  | 'Open / Zit-Sta';

export type WorkplaceType = 'Focus' | 'Routine' | 'Samenwerken';

export interface Workplace {
  /**
   * The ID of the workplace in the Appsemble API.
   */
  id?: number;

  /**
   * The code used to identify the worplace.
   */
  workplaceCode: string;

  /**
   * The name of the workplace that is displayed to the user.
   */
  workplaceName: string;

  /**
   * The capacity of the workplace.
   */
  capacity: number;

  /**
   * Whether this is a focus or a routine workplace.
   */
  workplaceType: WorkplaceType;

  /**
   * Whether the desks at this workplace support standing or just sitting.
   */
  deskType: DeskType;

  /**
   * Maintenance windows during which capacity is reduced.
   */
  maintenance?: MaintenanceWindow[];
}
