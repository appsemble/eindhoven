# Scripts

This package contains custom Node.js scripts that can be used to work with the Appsemble apps in
this repository.

## Usage

Each script can be run using:

```sh
npm run [SCRIPT_NAME]
```

Scripts use the `CLIENT_CREDENTIALS` environment variable for authentication. Client credentials can
be created at <https://appsemble.app/settings/client-credentials>.

### `sync`

Synchronize an XLSX file containing workspace information with Appsemble.

```sh
npm run sync [OPTIONS]... FILE...
```

#### Options

- `--id`: Specify the app ID to use. (Default: `292`)
- `--remote`: Specify the Appsemble remote to use. (Default: `https://appsemble.app`)
- `--help`: Print a help message.

#### Positionals

This script takes a positional argument which points to the XLSX file to load. The latest data was
taken from <https://gitlab.com/appsemble/eindhoven/-/issues/101#note_1264306156>.

## LICENSE

[LGPL-3.0-only](../../LICENSE.md) © [Appsemble](https://appsemble.com)
