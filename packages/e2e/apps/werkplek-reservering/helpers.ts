import { type ElementHandle } from '@playwright/test';

export function setTestDate(): Date {
  let date = new Date();

  const dayOfWeek = date.getDay();
  date = new Date(date.setDate(date.getDate() + (8 - dayOfWeek)));
  date.setHours(11);
  date.setMinutes(0);
  return date;
}

export function formatDate(date: Date, minutes = true): string {
  let formattedDate = `${date.getMonth() + 1}/${date.getDate()}/${String(date.getFullYear()).slice(
    2,
  )}`;
  if (minutes) {
    formattedDate += ` | ${date.getHours() % 12}:${date.getMinutes()}`;
  }

  return formattedDate;
}

export async function getTableRowByDate(
  formattedDate: string,
  tableRows: ElementHandle<HTMLElement | SVGElement>[],
): Promise<ElementHandle<HTMLElement | SVGElement>> {
  let row: ElementHandle<HTMLElement | SVGElement>;

  for (const tableRow of tableRows) {
    const cells = await tableRow.$$('td');
    const text = await (await cells[1].$('div'))!.textContent();
    if (text!.includes(formattedDate)) {
      row = tableRow;
      break;
    }
  }

  return row!;
}
