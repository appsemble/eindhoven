import { expect } from '@playwright/test';

import { test } from '../../fixtures/test/index.js';

const { BOT_ACCOUNT_EMAIL = 'bot@appsemble.com', BOT_ACCOUNT_PASSWORD = 'test' } = process.env;

const appPath = 'teambarometer';

const defaultTeamName = 'Test Group';

test.describe('Teambarometer', () => {
  test.skip();

  test.beforeEach(async ({ loginApp, studioLogin, visitApp }) => {
    await studioLogin();
    await visitApp(appPath);
    await loginApp();
  });

  test('should switch between team roles', async ({ changeGroupRole, page }) => {
    await changeGroupRole(appPath, defaultTeamName, 'manager');
    await page.waitForSelector('text=Create survey');

    await changeGroupRole(appPath, defaultTeamName, 'member');
    await page.waitForSelector('text=Create survey', { state: 'hidden' });
  });

  test.describe('Survey flow', () => {
    const testSurveyId = `Test Survey ${Date.now()}`;

    test.beforeEach(async ({ changeGroupRole, page }) => {
      await changeGroupRole(appPath, defaultTeamName, 'manager');
      await page.waitForLoadState('domcontentloaded');
      await page.getByRole('link', { name: 'Results' }).click();
      if (await page.getByLabel('Dismiss message').isVisible({ timeout: 5000 })) {
        await page.getByLabel('Dismiss message').click();
      }
      await page.getByLabel('Dismiss message').waitFor({ state: 'hidden' });
      await page.waitForSelector('text=Create survey');
    });

    test('should create a survey', async ({ page }) => {
      await page.getByRole('link', { name: '+ Create survey' }).click();
      await page.getByRole('button', { name: 'New survey' }).click();
      await page.getByPlaceholder('Name survey').fill(testSurveyId);
      await page
        .getByPlaceholder('Welcome message for employees')
        .fill('Welcome to the test survey');
      await page.getByRole('button', { name: 'Add questions >' }).click();
      await page.getByPlaceholder('empty').fill('This is an open question');
      // Questions are open by default
      // this actually acts as a toggle, not a select, even though it looks like a select
      // meaning if we click on the 'open' button it'll actually make the question multiple choice
      await page.getByRole('button', { name: '+ Add question' }).click();
      await page.getByPlaceholder('empty').last().fill('This is a multiple choice question');
      await page.getByRole('button', { name: 'open' }).last().click();
      await page.getByRole('button', { name: 'meerkeuze', exact: true }).click();
      // Questions are written to local storage (indexedDB), so we're going to wait for that
      // eslint-disable-next-line playwright/no-wait-for-timeout
      await page.waitForTimeout(1000);
      await page.getByRole('button', { name: 'Send control to >' }).click();
      await page.locator('#team').selectOption(defaultTeamName);
      // eslint-disable-next-line playwright/no-wait-for-timeout
      await page.waitForTimeout(1000);
      await page.getByRole('button', { name: 'Create >' }).click();

      await page.waitForSelector(`text=${testSurveyId}`);
      await expect(
        page.getByRole('row', { name: testSurveyId }).getByRole('gridcell').nth(1),
      ).toHaveText('Concept');
    });

    test('should schedule a survey', async ({ page }) => {
      await page.getByRole('link', { name: '+ Create survey' }).click();

      await page.getByRole('row', { name: testSurveyId }).getByRole('button').click();
      await page.getByRole('button', { name: 'Plan' }).click();
      await page.getByLabel('Repeat', { exact: true }).selectOption('Weekly');
      await page.getByLabel('Stop repeat', { exact: true }).selectOption('Date');

      await page.getByPlaceholder('Stop repeat date').click();
      await page.locator('.flatpickr-day.today + .flatpickr-day + .flatpickr-day').first().click();

      await page.getByPlaceholder('Start').click();
      await page.locator('.flatpickr-day.today').nth(1).click();

      await page.getByPlaceholder('Until').click();
      await page.locator('.flatpickr-day.today + .flatpickr-day').nth(2).click();

      await page.getByRole('button', { name: 'Plan in' }).click();

      await page.waitForSelector(`text=${testSurveyId}`);
      await expect(
        page.getByRole('row', { name: testSurveyId }).getByRole('gridcell').nth(1),
      ).toHaveText('Ingepland');
    });

    test('should set reminders', async ({ page }) => {
      await page.getByRole('link', { name: '+ Create survey' }).click();
      await page.getByRole('row', { name: testSurveyId }).getByRole('button').click();
      await page.getByRole('button', { name: 'Reminders' }).click();
      await page.getByRole('link', { name: 'Add notification' }).click();
      await page.getByPlaceholder('Title notification').fill('You are being notified');
      await page.getByPlaceholder('Start').click();
      await page.locator('.flatpickr-day.today + .flatpickr-day').click();
      await page.getByPlaceholder('here the text to go in the notification').fill('Hello, world');
      await page.getByRole('button', { name: 'Add notification' }).click();
      await page.getByRole('gridcell', { name: 'You are being notified' }).first().waitFor();
    });

    test('should take a survey', async ({ page }) => {
      await page.getByRole('link', { name: /Teambarometer/ }).click();
      await expect(page.getByRole('heading', { name: testSurveyId })).toBeVisible();
      await page.getByRole('link', { name: 'Start' }).click();
      await page.getByPlaceholder('Comment').fill('Hello, world');
      await page.getByRole('button', { name: 'Next question' }).click();

      await page.locator('.fa-face-frown').click();
      await page.getByPlaceholder('Comment').fill('Frown');

      await page.getByRole('button', { name: 'Done' }).click();

      await page.waitForSelector('text=Thank you');
    });

    test('should show survey results', async ({ baseURL, page }) => {
      // To view a survey's results, we need at least 5 people to have answered it
      // So we'll just create 4 more answers via the API
      const apps = await fetch(`${baseURL}/api/apps`).then<
        {
          id: number;
          path: string;
        }[]
      >((res) => res.json());
      expect(apps).toBeInstanceOf(Array);
      const appId = apps.find((app) => app.path === appPath)?.id;
      expect(appId).not.toBeUndefined();
      const credentials = await fetch(`${baseURL}/api/login`, {
        method: 'POST',
        headers: {
          Authorization: `Basic ${Buffer.from(
            `${BOT_ACCOUNT_EMAIL}:${BOT_ACCOUNT_PASSWORD}`,
          ).toString('base64')}`,
        },
        body: 'grant_type=client_credentials&scope=resources%3Awrite+resources%3Aread',
      })
        .then((res) => res.json() as Promise<{ access_token: string }>)
        .then((res) => res.access_token);

      const surveyId = await fetch(
        `${baseURL}/api/apps/${appId}/resources/survey?%24filter=title%20eq%20%27${testSurveyId}%27`,
        {
          headers: {
            Authorization: `Bearer ${credentials}`,
          },
        },
      )
        .then((res) => res.json() as Promise<{ id: number }[]>)
        .then((res) => res[0]?.id);

      expect(surveyId).not.toBeUndefined();

      for (const [answer, comment] of [
        ['happy', 'happy happy happy'],
        ['sad', 'Frown'],
        ['neutral', 'Mehh'],
        ['happy', 'Very happy'],
      ]) {
        const res = await fetch(`${baseURL}/api/apps/${appId}/resources/answers`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${credentials}`,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            surveyId,
            answers: [
              {
                question: 'This is an open question',
                comment,
              },
              {
                question: 'This is a multiple choice question',
                answer,
                comment,
              },
            ],
          }),
        });
        expect(res.status).toBe(201);
      }

      // We don't want to wait a day for the survey to close itself
      await fetch(`${baseURL}/api/apps/${appId}/resources/survey/${surveyId}`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${credentials}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          status: 'Closed',
        }),
      }).then((res) => expect(res.status).toBe(200));

      await page.reload();
      await page.waitForLoadState('domcontentloaded');
      await page
        .getByRole('row', { name: testSurveyId })
        .getByRole('button', { name: 'Results' })
        .click();

      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await expect(page.locator('canvas')).toHaveScreenshot({
        maxDiffPixelRatio: 0.05,
        timeout: 5000,
      });
      await page.locator('canvas').click({
        position: {
          x: 280,
          y: 100,
        },
      });
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await page.locator('h2[data-content="title"]:has-text("Comment cloud")').waitFor();
      await page.reload();
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
      await page.locator('h2[data-content="title"]:has-text("Comment cloud")').waitFor();
      // Not testing for screenshot on purpose - the wordcloud block is not deterministic enough
      await page.locator('canvas').waitFor();
    });
  });
});
