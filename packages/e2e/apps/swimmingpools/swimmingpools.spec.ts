import { expect } from '@playwright/test';

import { test } from '../../fixtures/test/index.js';

const { BOT_ACCOUNT_EMAIL = 'bot@appsemble.com', BOT_ACCOUNT_PASSWORD = 'test' } = process.env;

const appPath = 'swimming-pools-app';

test.describe('Swimmingpools', () => {
  test.beforeEach(async ({ loginApp, studioLogin, visitApp }) => {
    await studioLogin();
    await visitApp(appPath);
    await loginApp();
  });

  test('should fill in a report in the first pool', async ({ baseURL, page }) => {
    await page.locator('.appsemble-loader').waitFor({ state: 'hidden' });

    // Main page
    await page.click('table > tbody > tr > td:nth-child(2) > div > button');

    // General data - General data
    await page.fill('#madeBy', 'Bot');
    await page.fill('#visualInspectBy', 'Bot');
    await page.fill('#irregularities', 'Test Irregularities');
    await page.fill('#personalFeedback', 'Test Personal Feedback');
    await page.fill('#extraActivitiesClean', 'Test Extra Activities Clean');
    await page.click('form > div.snTkd.I0Yzs.mt-4 > button');

    // General data - Visual inspection
    await page.click('form > div:nth-child(3) > div:nth-child(1) > div > div:nth-child(1) > label');
    await page.click('form > div:nth-child(3) > div:nth-child(2) > div > div:nth-child(1) > label');
    await page.click('form > div:nth-child(3) > div:nth-child(3) > div > div:nth-child(1) > label');
    await page.fill('#visInspComments', 'Test Comments of visual inspections');
    await page.click('form > div.snTkd.I0Yzs.mt-4 > button');

    // General data - Personnel dispatch
    await page.fill('#schalen\\.0\\.timeOfDayFrom', '12:30');
    await page.fill('#schalen\\.0\\.timeOfDayUntil', '13:00');
    await page.fill('#schalen\\.0\\.swimmersAmnt', '100');
    await page.fill('#schalen\\.0\\.fromTzh', '1');
    await page.fill('#schalen\\.0\\.toTzh', '2');
    await page.fill('#schalen\\.0\\.consultWith', 'Bot');
    await page.click('form > div:nth-child(3) > fieldset > div.snTkd.I0Yzs.mt-2 > button');
    await page.click('form > div:nth-child(3) > fieldset > div.snTkd.I0Yzs.mt-2 > button');
    await page.click(
      'form > div:nth-child(3) > fieldset > div:nth-child(4) > div.snTkd.I0Yzs > button',
    );
    await page.fill('#schalen\\.1\\.timeOfDayFrom', '13:00');
    await page.fill('#schalen\\.1\\.timeOfDayUntil', '13:30');
    await page.fill('#schalen\\.1\\.swimmersAmnt', '50');
    await page.fill('#schalen\\.1\\.fromTzh', '2');
    await page.fill('#schalen\\.1\\.toTzh', '1');
    await page.fill('#schalen\\.1\\.consultWith', 'Bot');
    await page.fill('#noShow', 'Test No-Show Rentals');
    await page.fill('#incidentRent', 'Test Occasional Rentals');
    await page.click('form > div.snTkd.I0Yzs.mt-4 > button');

    // General data - Rescues
    await page.fill('#rescueFieldset\\.0\\.whoRescued', 'Bot');
    await page.fill('#rescueFieldset\\.0\\.placeResc', 'Home');
    await page.fill('#rescueFieldset\\.0\\.timeResc', '12:30');
    await page.fill('#rescueFieldset\\.0\\.otherPartInvolved', 'Bot');
    await page.click(
      'form > div:nth-child(3) > fieldset > div:nth-child(2) > div > div.field.appsemble-radio > div > div:nth-child(1) > label',
    );
    await page.click('form > div:nth-child(3) > fieldset > div.snTkd.I0Yzs.mt-2 > button');
    await page.click('form > div:nth-child(3) > fieldset > div.snTkd.I0Yzs.mt-2 > button');
    await page.click(
      'form > div:nth-child(3) > fieldset > div:nth-child(4) > div.snTkd.I0Yzs > button',
    );
    await page.fill('#rescueFieldset\\.1\\.whoRescued', 'Bot');
    await page.fill('#rescueFieldset\\.1\\.placeResc', 'Home');
    await page.fill('#rescueFieldset\\.1\\.timeResc', '13:00');
    await page.fill('#rescueFieldset\\.1\\.otherPartInvolved', 'Bot');
    await page.click(
      'form > div:nth-child(3) > fieldset > div:nth-child(3) > div:nth-child(1) > div.field.appsemble-radio > div > div:nth-child(2) > label',
    );
    await page.click('form > div.snTkd.I0Yzs.mt-4 > button');

    // Complete report
    await page.locator("text='Complete report'").waitFor({ state: 'visible' });
    await page.click("text='Complete report'");

    await page.locator('.appsemble-loader').waitFor({ state: 'hidden' });
    await page.click('span:has-text("Close Daily Report")');

    const apps = await fetch(`${baseURL}/api/apps`).then<{ id: number; path: string }[]>((res) =>
      res.json(),
    );

    const appId = apps.find((app) => app.path === appPath)?.id;

    const credentials = await fetch(`${baseURL}/api/login`, {
      method: 'POST',
      headers: {
        Authorization: `Basic ${Buffer.from(
          `${BOT_ACCOUNT_EMAIL}:${BOT_ACCOUNT_PASSWORD}`,
        ).toString('base64')}`,
      },
      body: 'grant_type=client_credentials&scope=resources%3Awrite+resources%3Aread',
    })
      .then((res) => res.json() as Promise<{ access_token: string }>)
      .then((res) => res.access_token);

    const reports = await fetch(`${baseURL}/api/apps/${appId}/resources/poolReports`, {
      headers: {
        Authorization: `Bearer ${credentials}`,
      },
    }).then((res) => res.json() as Promise<unknown[]>);

    expect(reports[0]).toMatchObject({
      rescues: [
        {
          whoRescued: 'Bot',
          placeResc: 'Home',
          timeResc: '12:30',
          otherPartInvolved: 'Bot',
          rescueType: 'wetRes',
        },
        {
          whoRescued: 'Bot',
          placeResc: 'Home',
          timeResc: '13:00',
          otherPartInvolved: 'Bot',
          rescueType: 'nearDrown',
        },
      ],
      madeBy: 'Bot',
      visualInspectBy: 'Bot',
      irregularities: 'Test Irregularities',
      personalFeedback: 'Test Personal Feedback',
      extraActivitiesClean: 'Test Extra Activities Clean',
      isEnoughCeilingPlates: true,
      isEnoughHangParts: true,
      isEnough25mBath: true,
      visInspComments: 'Test Comments of visual inspections',
      schalen: [
        {
          timeOfDayFrom: '12:30',
          timeOfDayUntil: '13:00',
          swimmersAmnt: '100',
          fromTzh: '1',
          toTzh: '2',
          consultWith: 'Bot',
        },
        {
          timeOfDayFrom: '13:00',
          timeOfDayUntil: '13:30',
          swimmersAmnt: '50',
          fromTzh: '2',
          toTzh: '1',
          consultWith: 'Bot',
        },
      ],
      noShow: 'Test No-Show Rentals',
      incidentRent: 'Test Occasional Rentals',
    });
  });
});
