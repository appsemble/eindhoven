import { type App } from '@appsemble/types';
import { test as base, expect } from '@playwright/test';

const { BOT_ACCOUNT_EMAIL = 'bot@appsemble.com', BOT_ACCOUNT_PASSWORD = 'test' } = process.env;

interface Fixtures {
  /**
   * Visit an app.
   *
   * Uses to values from the Appsemble class.
   *
   * @param appPath The path of the app to visit.
   */
  visitApp: (appPath: string) => Promise<void>;

  /**
   * Set the role of the user.
   *
   * @param app The app to set the role for.
   * @param role The role to set.
   *
   *   Note that this requires the user to be logged in to the studio.
   */
  changeRole: (app: string, role: string) => Promise<void>;

  /**
   * Set the role of the user in a team.
   *
   * @param app The app to set the role for.
   * @param group The group to set the role for.
   * @param role The role to set.
   */
  changeGroupRole: (app: string, group: string, role: 'manager' | 'member') => Promise<void>;

  /**
   * Login to the Appsemble studio.
   */
  studioLogin: () => Promise<void>;

  /**
   * Login to an Appsemble app.
   *
   * Uses to values from the Appsemble class.
   */
  loginApp: () => Promise<void>;
}

const organization = 'eindhoven';

export const test = base.extend<Fixtures>({
  async visitApp({ baseURL, page }, use) {
    await use(async (appPath: string) => {
      await page.goto(
        `http://${appPath}.${organization}.${new URL(baseURL ?? 'http://localhost:9999').host}`,
      );
    });
  },

  async studioLogin({ page }, use) {
    await use(async () => {
      const queryParams = new URLSearchParams({ redirect: '/en/apps' });
      await page.goto(`/en/login?${String(queryParams)}`);

      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      await page.getByTestId('email').fill(BOT_ACCOUNT_EMAIL);
      await page.getByTestId('password').fill(BOT_ACCOUNT_PASSWORD);
      await page.getByTestId('login').click();

      await expect(page).toHaveURL('/en/apps');
    });
  },

  async changeRole({ page }, use) {
    await use(async (app: string, role: string) => {
      const redirect = page.url();
      await page.goto(`/en/organizations/${organization}`);
      await page.click(`text=${app}`);
      const [path] = new URL(page.url()).hostname.split('.');
      const response = await fetch('/api/apps');
      const apps = (await response.json()) as App[];
      const index = apps.findIndex((item) => item.path === path);
      const appId = apps[index].id;
      await page.goto(`/en/apps/${appId}/users`);
      const select = page.locator('tr', { hasText: 'It’s you!' }).locator('select[class=""]');
      await select.selectOption(role);
      await page.goto(redirect);
    });
  },

  async changeGroupRole({ page }, use) {
    await use(async (app: string, group: string, role: 'manager' | 'member') => {
      const redirect = page.url();
      await page.goto(`/en/organizations/${organization}`);
      await page.click(`text=${app}`);
      const [path] = new URL(page.url()).hostname.split('.');
      const response = await fetch('/api/apps');
      const apps = (await response.json()) as App[];
      const index = apps.findIndex((item) => item.path === path);
      const appId = apps[index].id;
      await page.goto(`/en/apps/${appId}/groups`);
      await page.click(`text=${group}`);
      await page.getByRole('row', { name: 'Bot' }).locator('#role').selectOption(role);
      await page.goto(redirect);
    });
  },

  async loginApp({ page }, use) {
    await use(async () => {
      await page.addLocatorHandler(page.getByTestId('allow'), async () => {
        await page.getByTestId('allow').click();
      });
    });
  },
});
