import { type ParameterizedContext } from 'koa';

export interface Argv {
  /**
   * The port to listen on.
   *
   * @default 3000
   */
  port: number;

  /**
   * The secret key to use for securing the API.
   */
  secret?: string;

  /**
   * Increase the verbosity of the logger.
   */
  verbose: number;

  /**
   * Decrease the verbosity of the logger.
   */
  quiet: number;

  /**
   * The Appsemble host where the app using this API is running.
   *
   * @default "https://appsemble.app"
   */
  remote?: string;

  /**
   * The ID of the app that will use this API
   */
  appId: number;
}

export interface CustomT<Params extends Record<string, unknown> = Record<string, unknown>> {
  argv: Argv;
  params: Params;
}

export type Context<Params extends Record<string, unknown> = Record<string, unknown>> =
  ParameterizedContext<unknown, CustomT<Params>>;
