import { bootstrap } from '@appsemble/sdk';
import { type Workplace } from '@eindhoven/types';
import {
  add,
  areIntervalsOverlapping,
  clamp,
  eachMinuteOfInterval,
  isWithinInterval,
  parse,
  roundToNearestMinutes,
} from 'date-fns';

import { buildQuery } from './odata.js';
import { type Preferences, type Reservation } from './types.js';
import { findWorkplace, getWorkplaceCapacity } from './utils.js';

bootstrap(({ actions, events, pageParameters, parameters, utils }) => {
  const workplacesPromise = actions.getWorkplaces<Workplace[]>();
  const reservationsPromise = actions.getOwnReservations<Reservation[]>();
  const isCreating = pageParameters?.id == null;
  const getPreferencesPromise = isCreating ? actions.getPreferences<Preferences | null>() : null;
  const allReservationsPromise = actions.getAllReservations<Reservation[]>();

  function generateTimeIntervals(referenceDate: Date): Promise<boolean> {
    const date = new Date(referenceDate);
    const minTime = parse(parameters.minTime, 'HH:mm', date);
    const maxTime = parse(parameters.maxTime, 'HH:mm', date);

    if (!isWithinInterval(date, { start: minTime, end: maxTime })) {
      return events.emit.time(null, utils.formatMessage('invalidDate'));
    }

    // The date picker is supposed to pick from blocks of 15 minutes,
    // but manual entry is still possible.
    const startTime = roundToNearestMinutes(date, { nearestTo: 15 });
    const intervals =
      startTime.getTime() === maxTime.getTime()
        ? []
        : eachMinuteOfInterval({ start: startTime, end: maxTime }, { step: 15 }).slice(1);

    return events.emit.time(
      intervals.map((interval, index) => ({
        label: utils.formatMessage('intervalTime', { date: interval, duration: (index + 1) * 15 }),
        value: interval.toISOString(),
      })),
    );
  }

  if (isCreating && getPreferencesPromise) {
    getPreferencesPromise.then(async (preferences) => {
      let date = roundToNearestMinutes(add(new Date(), { minutes: 15 }), { nearestTo: 15 });
      let minTime = parse(parameters.minTime, 'HH:mm', date);
      let maxTime = parse(parameters.maxTime, 'HH:mm', date);

      if (date > maxTime) {
        // If the current time is later than the maximum allowed time,
        // set the default day to the minimum time + 2 hours.
        date = add(minTime, { days: 1, hours: 2 });
        minTime = date;
        maxTime = add(maxTime, { days: 1 });
      }

      const endDate = clamp(add(date, { hours: 4 }), { start: date, end: maxTime }).toISOString();

      await (preferences == null
        ? events.emit.preferences({
            date: date.toISOString(),
            endDate,
          })
        : events.emit.preferences({
            date: date.toISOString(),
            endDate,
            deskType: preferences.deskType,
            workplaceType: preferences.workplaceType,
          }));

      await generateTimeIntervals(date);
    });
  }

  /**
   * Validates whether there is room in the specified workplace for another reservation.
   *
   * @param reservation The reservation to validate the workplace for.
   * @returns Remapper result containing an error response.
   */
  async function validateCapacity(reservation: Reservation): Promise<string | undefined> {
    const workplaces = await workplacesPromise;
    const workplace = workplaces.find((w) => w.workplaceCode === reservation.workplaceCode);

    if (!workplace) {
      return utils.formatMessage('maxCapacityError');
    }

    let { capacity, maintenance } = workplace;
    if (maintenance?.length) {
      const reservationInterval = {
        start: new Date(reservation.date),
        end: new Date(reservation.endDate),
      };
      for (const maintenanceWindow of maintenance) {
        const maintenanceInterval = {
          start: new Date(maintenanceWindow.start),
          end: new Date(maintenanceWindow.end),
        };
        if (areIntervalsOverlapping(reservationInterval, maintenanceInterval)) {
          capacity = Math.min(capacity, maintenanceWindow.capacity);
        }
      }
    }

    if (capacity < 1) {
      return utils.formatMessage('maxCapacityError');
    }

    const count = await actions.count<number>({ query: buildQuery(reservation) });

    if (count < capacity) {
      // Capacity not reached on the selected date, time, and location.
      return;
    }

    return utils.formatMessage('maxCapacityError');
  }

  /**
   * Validates whether there is already an ongoing reservation
   * based on the given interval and list of existing reservations.
   *
   * @param reservation The reservation to validate the starting and end times of.
   * @returns Remapper result containing an error response.
   */
  async function validateDate(reservation: Reservation): Promise<string | undefined> {
    let ownReservations = (await reservationsPromise).filter((r) => !r.checkout);
    if (pageParameters?.id) {
      // If we’re editing a resource,
      // we should ignore the reservation that’s currently being edited.
      ownReservations = ownReservations.filter((r) => r.id !== Number(pageParameters.id));
    }

    const start = new Date(reservation.date);
    const end = new Date(reservation.endDate || reservation.date);

    if (start.getTime() >= end.getTime()) {
      return;
    }

    const currentInterval = { start, end };

    if (
      ownReservations.some((r) =>
        areIntervalsOverlapping(currentInterval, {
          start: new Date(r.date),
          end: new Date(r.endDate),
        }),
      )
    ) {
      return utils.formatMessage('invalidDate');
    }
  }

  events.on.validateAll(async (reservation: Reservation) => {
    const capacityError = await validateCapacity(reservation);
    if (capacityError) {
      await events.emit.validatedAll(null, capacityError);
      return;
    }

    const dateError = await validateDate(reservation);
    if (dateError) {
      await events.emit.validatedAll(null, dateError);
      return;
    }

    const workplace = await findWorkplace(workplacesPromise, reservation.workplaceCode);
    await events.emit.validatedAll({
      ...reservation,
      workplaceName: workplace?.workplaceName,
    });

    if (isCreating && getPreferencesPromise) {
      const preferences = await getPreferencesPromise;
      const updatedPreferences: Preferences = {
        deskType: reservation.deskType,
        workplaceType: reservation.workplaceType,
      };

      await (preferences == null
        ? actions.createPreferences(updatedPreferences)
        : actions.updatePreferences({ ...updatedPreferences, id: preferences.id }));
    }
  });

  events.on.dateChanged(async (data: Reservation) => {
    if (data.id == null) {
      await generateTimeIntervals(new Date(data.date));
    } else {
      const reservations = await reservationsPromise;
      const originalReservation = reservations.find((r) => r.id === data.id);
      await generateTimeIntervals(
        new Date(originalReservation ? originalReservation.endDate : data.date),
      );
    }

    // This clears any previously generated errors.
    const message = await validateDate(data);
    if (message) {
      return events.emit.validatedDate(null, message);
    }

    await events.emit.validatedDate(null);
  });

  events.on.changed(async (data: Reservation) => {
    if (!data.workplaceCode) {
      return;
    }
    const result = await validateCapacity(data);

    await (result
      ? events.emit.validatedReservation(null, result)
      : events.emit.validatedReservation({}));
  });

  events.on.typesSelected(async (reservation: Reservation) => {
    const workplaces = await workplacesPromise;
    const filtered = workplaces.filter(
      (workplace) =>
        workplace.workplaceType === reservation.workplaceType &&
        workplace.deskType === reservation.deskType &&
        getWorkplaceCapacity(workplace, reservation) > 0,
    );

    if (!filtered.length) {
      await events.emit.workplaces([]);
      return;
    }

    const start = new Date(reservation.date);
    const end = new Date(reservation.endDate);

    if (start.getTime() >= end.getTime()) {
      await events.emit.workplaces([]);
      return;
    }

    const allReservations = await allReservationsPromise;
    const interval = { start, end };
    const locationEntries = filtered.map((workplace) => {
      const totalCapacity = getWorkplaceCapacity(workplace, reservation);
      let reservationCount = 0;

      for (const r of allReservations) {
        if (r.workplaceCode !== workplace.workplaceCode) {
          continue;
        }

        if (
          areIntervalsOverlapping(interval, {
            start: new Date(r.date),
            end: new Date(r.checkout ?? r.endDate),
          })
        ) {
          reservationCount += 1;
        }
      }

      const remainingCapacity = Math.max(totalCapacity - reservationCount, 0);

      return {
        label: utils.formatMessage('workplaceAvailability', {
          workplaceName: workplace.workplaceName,
          remainingCapacity,
          totalCapacity,
        }),
        disabled: remainingCapacity === 0,
        value: workplace.workplaceCode,
      };
    });

    await events.emit.workplaces(
      locationEntries.sort((a, b) => {
        if (a.disabled === b.disabled) {
          return a.value.localeCompare(b.value);
        }
        return a.disabled ? 1 : -1;
      }),
    );
  });
});
