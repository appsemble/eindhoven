import { type Reservation } from './types.js';

/**
 * Build an OData query to search for reservation conflicts.
 *
 * @param reservation A reservation to create a conflict filter for.
 * @returns An OData query.
 */
export function buildQuery({ date, endDate, id, workplaceCode }: Reservation): string {
  const query = [
    `date ge '${new Date(date).toISOString()}' and endDate le '${new Date(endDate).toISOString()}'`,
  ];

  if (id != null) {
    query.push(`id ne ${id}`);
  }

  if (workplaceCode) {
    query.push(`workplaceCode eq '${workplaceCode}'`);
  }

  return query.join(' and ');
}
