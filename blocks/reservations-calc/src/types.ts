import { type DeskType, type WorkplaceType } from '@eindhoven/types';

/**
 * The settings the user can save when creating new reservations.
 */
export interface Preferences {
  id?: number;
  workplaceType: WorkplaceType;
  deskType: DeskType;
}

/**
 * A partial reservation for the form.
 */
export interface Reservation {
  /**
   * The ID of the resource that’s being modified.
   */
  id?: number;

  /**
   * The date of a reservation.
   */
  date: string;

  /**
   * The time of the reservation.
   */
  endDate: string;

  /**
   * The type of desk being used.
   */
  deskType: DeskType;

  /**
   * The type of the workplace.
   */
  workplaceType: WorkplaceType;

  /**
   * The workplace of a reservation.
   */
  workplaceCode: string;

  /**
   * The user-facing name of the workplace.
   */
  workplaceName: string;

  /**
   * The time the user checked out early.
   */
  checkout?: string;

  /**
   * The time the user checked in.
   */
  checkin?: string;
}

export interface Team {
  /**
   * The ID of the team
   */
  id: number;

  /**
   * The name of the team.
   */
  name: string;

  /**
   * The role of the user within the team.
   */
  role?: 'manager' | 'member';

  /**
   * The total amount of members of the team.
   */
  size: number;

  /**
   * The custom annotations in the team.
   */
  annotations?: Record<string, string>;
}
