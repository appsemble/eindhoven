import { type Workplace } from '@eindhoven/types';
import { areIntervalsOverlapping } from 'date-fns';

import { type Reservation } from './types.js';

/**
 * Find a workplace based on its code.
 *
 * @param workplacesPromise The promise which resolves with all workplaces.
 * @param workplaceCode The code of the workplace to find.
 * @returns The found building
 */
export async function findWorkplace(
  workplacesPromise: Promise<Workplace[]>,
  workplaceCode: string,
): Promise<Workplace | undefined> {
  const buildings = await workplacesPromise;
  return buildings.find((workplace) => workplace.workplaceCode === workplaceCode);
}

/**
 * Get the workplace capacity at the moment of a planned reservation.
 *
 * @param workplace The workplace to get the capacity of.
 * @param reservation Tje reservation that is being planned.
 * @returns The workplace capacity at the moment of the reservation.
 */
export function getWorkplaceCapacity(workplace: Workplace, reservation: Reservation): number {
  let { capacity, maintenance } = workplace;

  if (maintenance && reservation.date && reservation.endDate) {
    const reservationInterval = {
      start: new Date(reservation.date),
      end: new Date(reservation.endDate),
    };

    for (const window of maintenance) {
      const windowInterval = { start: new Date(window.start), end: new Date(window.end) };

      if (areIntervalsOverlapping(reservationInterval, windowInterval)) {
        capacity = Math.min(capacity, window.capacity);
      }
    }
  }

  return capacity;
}
