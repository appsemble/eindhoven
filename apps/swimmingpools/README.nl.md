De Appsemble Inspectie App is een veelzijdige tool ontworpen om het inspectieproces in verschillende
faciliteiten te stroomlijnen. Hoewel ons voorbeeld zich richt op zwembadinspecties, kan de
applicatie worden geconfigureerd om elke faciliteit te inspecteren, waardoor het een echt aanpasbare
oplossing is.

## Belangrijkste kenmerken

### Locatieselectie

Gebruikers kunnen kiezen uit meerdere locaties, zoals verschillende zwembaden binnen een gemeente.

### Gebiedselectie

Binnen elke locatie kunnen gebruikers specifieke gebieden selecteren (bijv. het 50m zwembad, het 25m
zwembad, enz.) uit een vooraf gedefinieerde lijst.

### Versiebeheer

De app is ontworpen om variaties in vragen tussen zwembaden te accommoderen, waardoor het mogelijk
is om meerdere versies van de app te onderhouden.

### Rapportage en goedkeuring

Het gegenereerde rapport moet door iemand anders worden goedgekeurd. De eerste beoordelaar
verifieert het rapport, ondertekent het en stuurt automatisch een e-mail naar de tweede persoon die
het rapport verifieert en ondertekent.

### Gebruikersbeheer

Er is een manier om gebruikers (ploegleiders) te beheren, inclusief gebruikers’ e-mail, naam, etc.

## Stromen

### Samenstellen dagrapport

Open app > selecteer de juiste locatie/zwembad > vul gedurende de dag informatie in > laatste
persoon stuurt e-mail

### Verifiëren dagrapport

Kijk naar e-mail > link naar het rapport in de app > bewerkt mogelijk het rapport waar nodig >
ondertekent het > app stuurt een nieuwe e-mail naar de tweede beoordelaar > dezelfde stappen…

### Configureren

Configureer team > bewerk persoon… etc.

### Locaties

Configureer locaties > locatie > zwembaden…

Deze app is bedoeld om het dagelijkse werk van zwembadmedewerkers te vergemakkelijken, die een of
meerdere keren per dag formulieren invullen voor veiligheid en voor het behouden van de kwalificatie
voor het kwaliteitsmerk. Het is een veelzijdige tool die kan worden aangepast aan de specifieke
behoeften van uw faciliteit. Met de Appsemble Inspectie App kunt u uw inspectieproces stroomlijnen
en efficiënter maken.
