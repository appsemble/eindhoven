Privacy Statement:

- Deze applicatie gebruikt alleen jouw e-mail adres om de reservering te registreren. Alle gegevens
  die je zelf toevoegt aan je persoonlijke profiel zijn optioneel en niet verplicht.
- Nadat je reservering is afgelopen wordt je e-mailadres binnen 24 uur losgekoppeld van de
  reservering.
- Er wordt een gebruikersprofiel aangemaakt waarin je persoonlijke voorkeuren worden opgeslagen. Die
  hoef je niet te gebruiken.
- Als je de applicatie niet gebruikt wordt je persoonlijke profiel na een maand automatisch
  verwijderd. Indien je daarna toch terug inlogt, wordt er een nieuw gebruikersprofiel aangemaakt
  zonder de oude instellingen.
- We hebben bij het ontwerpen van deze applicatie veel zorg besteed aan je privacy. Indien je toch
  ergens vragen over hebt of het ergens niet mee eens bent kan je je in eerste instantie richten tot
  [privacy.bv@einhoven.nl](mailto:privacy.bv@einhoven.nl).
- Meer informatie over de gegevens die we verwerken en jouw rechten m.b.t. privacy vind je in de
  specifieke privacyverklaring op Vibes:
