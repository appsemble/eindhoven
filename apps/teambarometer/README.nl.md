De “Teambarometer” is een app die wordt gebruikt om de betrokkenheid en het welzijn van teamleden te
meten. Er zijn twee sets vragen die worden gebruikt.

De eerste set bevat vragen over de betrokkenheid van het individu bij veranderingen in hun
werkomgeving, hun gevoel van informatievoorziening over sectorontwikkelingen, trots op hun werk,
teamwerk en hun huidige vitaliteit (fysieke en mentale fitheid). Er is ook een vraag waarin
teamleden worden aangemoedigd om verbeteringen te benoemen waaraan ze willen werken.

De tweede set bevat vragen die gericht zijn op het begrip van het teamlid van hoe hun werk bijdraagt
aan de doelstellingen van de organisatie, hun betrokkenheid bij het veranderprogramma binnen de
afdeling, hun perceptie van besluitvorming, teamdynamiek, samenwerking, persoonlijk enthousiasme en
vitaliteit.

De app kan het beste worden ingezet in een omgeving waar continue verbetering wordt aangemoedigd en
waar feedback wordt gewaardeerd. Het doel is om een positieve werkomgeving te bevorderen waar
teamleden zich betrokken, geïnformeerd en vitaal voelen.
